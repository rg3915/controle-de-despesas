## Este projeto foi feito com:

* [VueJS 2.6.11](https://vuejs.org/)

## Como rodar o projeto?

```
git clone https://gitlab.com/rg3915/controle-de-despesas.git
cd controle-de-despesas/frontend
npm install
npm run serve
```

Numa outra aba do terminal rode o json-server

```
cd controle-de-despesas
json-server --watch fixtures/db.json
```
