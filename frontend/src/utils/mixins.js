export const parseDateMixin = {
  methods: {
    formatDateToEdit (date) {
      if (!date) return null

      const [year, month, day] = date.split('-')
      return `${day}/${month}/${year}`
    },
    parseDate (date) {
      if (!date) return null

      let [theYear, theMonth, theDay] = date.split('-')

      if (date.includes('/')) [theDay, theMonth, theYear] = date.split('/')

      return `${theYear}-${theMonth.padStart(2, '0')}-${theDay.padStart(2, '0')}`
    },
  },
}
