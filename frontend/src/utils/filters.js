import Vue from 'vue'
import moment from 'moment'

Vue.filter('formatDate', function (value) {
  if (!value) return ''
  return moment(value).format('DD/MM/YYYY')
})

Vue.filter('formatPrice', function (value) {
  if (!value) return ''
  const val = (value / 1).toFixed(2).replace('.', ',')
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
})
