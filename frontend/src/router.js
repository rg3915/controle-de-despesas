import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import('@/views/dashboard/Index'),
      children: [
        // Dashboard
        {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        // Pages
        {
          name: 'User Profile',
          path: 'pages/user',
          component: () => import('@/views/dashboard/pages/UserProfile'),
        },
        {
          name: 'Notifications',
          path: 'components/notifications',
          component: () => import('@/views/dashboard/component/Notifications'),
        },
        {
          name: 'Icons',
          path: 'components/icons',
          component: () => import('@/views/dashboard/component/Icons'),
        },
        {
          name: 'Typography',
          path: 'components/typography',
          component: () => import('@/views/dashboard/component/Typography'),
        },
        // Tables
        {
          name: 'Regular Tables',
          path: 'tables/regular-tables',
          component: () => import('@/views/dashboard/tables/RegularTables'),
        },
        // Maps
        {
          name: 'Google Maps',
          path: 'maps/google-maps',
          component: () => import('@/views/dashboard/maps/GoogleMaps'),
        },
        // Upgrade
        {
          name: 'Upgrade',
          path: 'upgrade',
          component: () => import('@/views/dashboard/Upgrade'),
        },
        // Expenses
        {
          name: 'Criar Recebimento',
          path: '/pages/receipts-new',
          component: () => import('@/views/financial/pages/Receivable'),
        },
        {
          name: 'Criar Despesa',
          path: '/pages/expenses-new',
          component: () => import('@/views/financial/pages/Expense'),
        },
        {
          name: 'Previsão Orçamentária',
          path: '/pages/budget-forecast',
          component: () => import('@/views/financial/pages/BudgetForecasts'),
        },
        {
          name: 'Lista de Compras',
          path: '/pages/shopping-list',
          component: () => import('@/views/financial/pages/ShoppingLists'),
        },
        {
          name: 'Dívidas',
          path: '/pages/debts',
          component: () => import('@/views/financial/pages/Debts'),
        },
        {
          name: 'Contas a Pagar',
          path: '/pages/bills-to-pay',
          component: () => import('@/views/financial/pages/Expenses'),
        },
        {
          name: 'Contas a Receber',
          path: '/pages/bills-to-receive',
          component: () => import('@/views/financial/pages/Receivables'),
        },
        {
          name: 'Fluxo de Caixa',
          path: '/pages/reports/cash-flow',
          component: () => import('@/views/financial/pages/reports/CashFlow'),
        },
        {
          name: 'Bancos',
          path: '/pages/banks',
          component: () => import('@/views/financial/pages/BankAccounts'),
        },
        {
          name: 'Tipos de Despesas',
          path: '/pages/expense-types',
          component: () => import('@/views/financial/pages/ExpenseTypes'),
        },
        {
          name: 'Categorias',
          path: '/pages/categories',
          component: () => import('@/views/financial/pages/ExpenseCategories'),
        },
        // List
        {
          name: 'List',
          path: 'pages/list',
          component: () => import('@/views/dashboard/pages/List'),
        },
        // Detail
        {
          name: 'Detail',
          path: 'pages/detail',
          component: () => import('@/views/dashboard/pages/Detail'),
        },
        // Report
        {
          name: 'Report',
          path: 'pages/report',
          component: () => import('@/views/dashboard/pages/Report'),
        },
      ],
    },
  ],
})
